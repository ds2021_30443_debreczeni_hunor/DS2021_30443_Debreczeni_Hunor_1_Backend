package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.builders.SensorBuilder;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.SensorRepository;

@Service
public class SensorService extends CrudService<SensorDTO, Sensor> {
    @Autowired
    public SensorService(SensorBuilder sensorBuilder, SensorRepository sensorRepository) {
        super(sensorBuilder, sensorRepository);
    }
}
