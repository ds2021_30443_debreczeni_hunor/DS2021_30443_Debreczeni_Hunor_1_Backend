package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.repositories.DeviceRepository;

@Service
public class DeviceService extends CrudService<DeviceDTO, Device> {

    @Autowired
    public DeviceService(DeviceBuilder deviceBuilder, DeviceRepository deviceRepository) {
        super(deviceBuilder, deviceRepository);
    }
}
