package ro.tuc.ds2020.controllers;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.EntityValidationException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.HasId;
import ro.tuc.ds2020.services.CrudService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public abstract class CrudController<DTO extends HasId, Obj extends HasId> {
    private final CrudService<DTO, Obj> service;

    protected CrudController(CrudService<DTO, Obj> service) {
        this.service = service;
    }

    protected abstract String getTypeName();

    @GetMapping()
    public ResponseEntity<List<DTO>> readAll(Principal principal) {
        List<DTO> dtos = service.findAll();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> create(@Valid @RequestBody DTO dto) {
        try {
            dto = service.insert(dto);
        } catch (DataIntegrityViolationException e) {
            throw new EntityValidationException("Users", List.of(e.getRootCause().getMessage()));
        }
        return new ResponseEntity<>(dto.getId(), HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DTO> read(@PathVariable("id") UUID id) {
        Optional<DTO> dto = service.findById(id);
        if (dto.isEmpty()) {
            throw new ResourceNotFoundException(getTypeName() + " with id: " + id);
        }
        return new ResponseEntity<>(dto.get(), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<DTO> update(@Valid @RequestBody DTO userDTO) {
        final DTO updatedUserDTO;
        try {
            updatedUserDTO = service.update(userDTO);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException("User not found");
        }

        return new ResponseEntity<>(updatedUserDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Boolean> delete(Principal principal, @PathVariable("id") UUID userId) {
        try {
            service.deleteById(userId);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException("User not found");
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

}
