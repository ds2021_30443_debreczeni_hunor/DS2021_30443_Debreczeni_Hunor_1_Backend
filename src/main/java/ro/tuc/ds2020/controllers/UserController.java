package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ParameterValidationException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.entities.Role;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.services.UserService;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/user")
public class UserController extends CrudController<UserDTO, User> {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        super(userService);
        this.userService = userService;
    }

    @Override
    protected String getTypeName() {
        return User.class.getSimpleName();
    }

    @Override
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Boolean> delete(Principal principal, @PathVariable("id") UUID userId) {
        final User user = userService.findByEmail(principal.getName()).orElseThrow(() -> new ResourceNotFoundException("User not found"));
        if (user.getId().equals(userId)) {
            throw new ParameterValidationException("User delete action", List.of("You can't delete yourself"));
        }

        return super.delete(principal, userId);
    }

    @GetMapping("/roleCheck")
    public ResponseEntity<Boolean> roleCheck(Principal principal, @Param("roleName") String roleName) {
        final Optional<User> optionalUser = userService.findByEmail(principal.getName());
        if (optionalUser.isEmpty()) {
            return ResponseEntity.ok(false);
        }

        final Set<String> roles = optionalUser.get().getRoles().stream().map(Role::getName).collect(Collectors.toSet());
        return ResponseEntity.ok(roles.contains(roleName));
    }
}
