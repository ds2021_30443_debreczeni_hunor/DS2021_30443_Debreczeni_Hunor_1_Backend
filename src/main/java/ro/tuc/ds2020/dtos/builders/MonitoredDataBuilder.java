package ro.tuc.ds2020.dtos.builders;

import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.MonitoredDataDTO;
import ro.tuc.ds2020.entities.MonitoredData;

@Component
public final class MonitoredDataBuilder implements EntityBuilder<MonitoredDataDTO, MonitoredData> {
    private MonitoredDataBuilder() {
    }

    @Override
    public MonitoredDataDTO toDto(MonitoredData monitoredData) {
        return new MonitoredDataDTO(
                monitoredData.getDate(),
                monitoredData.getValue()
        );
    }

    @Override
    public MonitoredData toEntity(MonitoredDataDTO monitoredDataDTO) {
        return MonitoredData.builder()
                .date(monitoredDataDTO.getDate())
                .value(monitoredDataDTO.getValue())
                .build();
    }

    @Override
    public MonitoredData toEntity(MonitoredData original, MonitoredDataDTO monitoredDataDTO) {
        setIfPresent(monitoredDataDTO::getDate, original::setDate);
        setIfPresent(monitoredDataDTO::getValue, original::setValue);

        return original;
    }
}
