# Setup

1. PostgreSql Database must be set up **locally** with the following specifications:

- port: `5432`
- username: `root`
- password: `secret`
- database name: `city-db`

2. Have `mvn` locally and also `jdk11` and run the followings from the project folder:

- `mvn package`
- `java -jar target/ds-2020-0.0.1-SNAPSHOT.jar`

That's it.
